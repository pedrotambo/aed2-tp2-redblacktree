#include <iostream>
#include <gtest/gtest.h>
#include "map.h"

using namespace std;



int main(int argc, char* argv[]) {
  cout << "Hello World!" << endl;
  
  aed2::map<int, int> m;
  if (m.empty()) cout << "VACIO" << endl;
  if (m.size() == 0) cout << "0 Elementos" << endl;
  std::pair<int,int> b;
  std::pair<int,int> b2;
  std::pair<int,int> b3;
  std::pair<int,int> b4;
  b = std::make_pair (5,10);
  b2 = std::make_pair (7,8);
  b3 = std::make_pair (9,12);
  b4 = std::make_pair (77,90);
  aed2::map<int,int>::const_iterator it = m.begin();
  it = m.insert(b3);
  it = m.insert(b2);
  it = m.insert(b);
  it = m.insert(b3);
  it = m.insert(b3);
  it = m.insert(b3);
  it = m.insert(b3);
  it = m.insert(b3);
  it = m.insert(b3);
  it = m.insert(b3);
  it = m.insert(b3);
  cout << "Count?: " << m.size() << endl;
  it = m.insert(b4);


  aed2::map<int,int>::iterator e;
  e = m.find(5);

  if ( e == m.end() ){
  	cout << "MAL" << endl;
  } else {
  	cout << (*e).first << endl;
  }

  e = m.find(9);
  if ( e == m.end() ){
  	cout << "MAL" << endl;
  } else {
  	cout << (*e).first << endl;
  }
  e = m.find(77);
  if ( e == m.end() ){
  	cout << "MAL" << endl;
  } else {
  	cout << (*e).first << endl;
  }

  cout << "LOWER BOUND......" << endl;
  cout << (*m.lower_bound(5)).first << endl;
  cout << (*m.lower_bound(7)).first << endl;
  cout << (*m.lower_bound(9)).first << endl;
  cout << (*m.lower_bound(75)).first << endl;
  cout << (*m.lower_bound(77)).first << endl;

  cout << "AT ............." << endl;
  cout << "Significado de 5: " << m.at(5) << endl;
	cout << "Significado de 7: " << m.at(7) << endl;
	cout << "Significado de 9: " << m.at(9) << endl;
	cout << "Significado de 77: " << m.at(77) << endl;
	 	
	cout << (*m.insert(b)).first << endl;


  std::pair<int,int> a = std::make_pair (5,15);
	cout << "Insertando nuevo significado de: " << 
						   (*m.insert_or_assign(a)).first << endl;

	cout << "Nuevo significado de 5: " << (*m.find(5)).second << endl;





    m.insert(std::make_pair (12,145));
    m.insert(std::make_pair (25,315));
    m.insert(std::make_pair (2,115));
    m.insert(std::make_pair (1,154));



	cout << "Recorriendo árbol inorder......" << endl;
	for (aed2::map<int,int>::iterator it = m.begin() ; it != m.end() ; ++it){
		cout << "Key: " << (*it).first << ", " << (*it).second << endl;
	}

  cout << "Recorriendo árbol preorder....." << endl;
  aed2::map<int,int>::iterator it_r = m.end();
  for (--it_r ; it_r != m.begin() ; --it_r){
    cout << "Key: " << (*it_r).first << ", " << (*it_r).second << endl;
  }
  cout << "Key: " << (*it_r).first << ", " << (*it_r).second << endl;
  cout << "--------------------------------" << endl;

  auto iter = m.lower_bound(78);
  if (iter == m.end()) cout << "OK" << endl;
  //m.insert(iter, std::make_pair (78,154));
  m.insert(std::make_pair (78,154));
  iter = m.find(78);
  if (iter == m.end()) cout << "MAL";
  cout << "Funciona insert con hint para elemento máximo: " << (*iter).first << ", OK" << endl;


  cout << "Imprimo el árbol antes de empezar a borrar..." << endl;
  m.begin();
  m.erase(78);
  cout << "Borré el 78, ahora quedó" << endl;
  m.begin();
  cout << "Borrando todo el árbol... Resultado:" << endl;
  m.erase(77);
  m.erase(12);
  m.erase(2);
  m.erase(7);
  m.erase(5);
  m.erase(9);
  m.erase(1);
  m.erase(25);
  if (m.empty()) cout << "Árbol vacío" << endl;
  m.begin();
  cout << "Volviendo a insertar valores..." << endl;

  m.insert(std::make_pair (25,315));
  m.insert(std::make_pair (2,115));
  m.insert(std::make_pair (12,145));
  m.insert(std::make_pair (12,145));
  m.insert(std::make_pair (22,315));
  m.insert(std::make_pair (102,115));
  m.insert(std::make_pair (7,154));
  m.begin();
  cout << "Agrege " << m.size() << " elementos." << endl;

  cout << m[25] << endl;
  cout << m[2525] << endl;

  cout << "Recorriendo árbol inorder M......" << endl;
  for (aed2::map<int,int>::iterator it = m.begin() ; it != m.end() ; ++it){
    cout << "Key: " << (*it).first << ", " << (*it).second << endl;
  }

  cout << "Recorriendo árbol preorder M....." << endl;
  it_r = m.end();
  for (--it_r ; it_r != m.begin() ; --it_r){
    cout << "Key: " << (*it_r).first << ", " << (*it_r).second << endl;
  }
  cout << "Key: " << (*it_r).first << ", " << (*it_r).second << endl;
  cout << "--------------------------------" << endl;




  aed2::map<int,int>::const_iterator lb = m.lower_bound(5);
  std::pair<int,int> cinco = std::make_pair (5,2322);
  cout << (*m.lower_bound(5)).first << endl;
  lb = m.end();
  //lb = m.lower_bound(5);
  //cout << (*lb).first << endl;
  lb = m.insert(lb,cinco);
  cout << "KEY: " << (*m.find(5)).first << ", Elementos: " << m.size() << endl;
  lb = m.insert(lb,cinco);
  cout << "KEY: " << (*m.find(5)).first << ", Elementos: " << m.size() << endl;


  cout << (*m.insert(std::make_pair (1234,2322))).first << "==" << "1234" << endl;

  cout << "Copiando Diccionarios...." << endl;


  aed2::map<int, int> copia(m);
  cout << "Recorriendo árbol inorder la Copia......" << endl;
  for (aed2::map<int,int>::iterator it = copia.begin() ; it != copia.end() ; ++it){
    cout << "Key: " << (*it).first << ", " << (*it).second << endl;
  }

  cout << (*copia.find(12)).first << endl;

  
  bool asdf = copia < m;
  cout << asdf << endl;
  aed2::map<int,int> copia_asig;
  lb = copia_asig.insert(std::make_pair (1,2322));
  copia_asig = m;
 
 // lb = copia_asig.insert(std::make_pair (12312315,2322));
  cout << "Recorriendo árbol inorder de la copia asignada......" << endl;
  for (aed2::map<int,int>::iterator it = copia_asig.begin() ; it != copia_asig.end() ; ++it){
    cout << "Key: " << (*it).first << ", " << (*it).second << endl;
  }
  return 0;
}
